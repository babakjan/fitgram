//
//  NewCommentView.swift
//  Fitgram
//
//  Created by Jan Babák on 19.10.2022.
//

import SwiftUI

struct NewCommentView: View {
    @StateObject var viewModel: NewCommentViewModel
    
    var body: some View {
        VStack {
            TextField("New comment", text: $viewModel.comment)
                .padding(10)
                .overlay(
                    RoundedRectangle(cornerRadius: 6)
                        .strokeBorder(
                            Color.black,
                            style: StrokeStyle(lineWidth: 1.0)
                        )
                )
                .padding(.horizontal, 20)
        }
        
        if viewModel.isLoading {
            ProgressView().progressViewStyle(.circular)
        } else {
            Button("Add comment") {
                Task {
                    await viewModel.addComment()
                }
            }
            .font(.footnote.bold())
            .disabled(viewModel.comment.isEmpty)
        }
    }
}

struct NewCommentView_Previews: PreviewProvider {
    static var previews: some View {
        
        NewCommentView(
            viewModel: NewCommentViewModel(
                postID: "",
                isPresented: .constant(false),
                onNewComment: { _ in }
            )
        )
    }
}
