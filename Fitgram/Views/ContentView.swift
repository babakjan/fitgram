//
//  ContentView.swift
//  Fitgram
//
//  Created by Jan Babák on 12.10.2022.
//

import SwiftUI

struct ContentView: View {
    var user = User(
        username: "honza_babak",
        nick: "Honza Babák",
        followers: 397,
        following: 122,
        profilePicture: "babakProfilePic",
        description: "🏫 FIT ČVUT -> webdev\n🛹🏓🏌️‍♂️🏀 sports\n🌍Prague Czech Republic\n 23 y/o",
        posts: []
    )
    
    var body: some View {
        TabView {
            PostsView()
                .tabItem {
                    Label("Feed", systemImage: "list.dash")
                }
            ProfileView(user: user)
                .tabItem {
                    Label("Profile", systemImage: "person")
                }
        }
        .onAppear {
            let tabBarAppearance = UITabBarAppearance()
            tabBarAppearance.configureWithDefaultBackground()
            UITabBar.appearance().scrollEdgeAppearance = tabBarAppearance
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .preferredColorScheme(.light)
        ContentView()
            .preferredColorScheme(.dark)
    }
}
