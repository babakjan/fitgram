//
//  Post.swift
//  Fitgram
//
//  Created by Jan Babák on 12.10.2022.
//

import Foundation

struct Post: Identifiable, Hashable {
    let id: String
    let likes: Int
    let photos: [String]
    let description: String
    let comments: Int
    let author: Author
}

extension Post: Codable {
    enum CodingKeys: String, CodingKey {
        case id
        case likes
        case photos
        case author
        case description = "text"
        case comments = "numberOfComments"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try container.decode(String.self, forKey: .id)
        self.photos = try container.decode([String].self, forKey: .photos)
        self.description = try container.decode(String.self, forKey: .description)
        self.comments = try container.decode(Int.self, forKey: .comments)
        self.author = try container.decode(Author.self, forKey: .author)
        let likes = try container.decode([String].self, forKey: .likes)
        self.likes = likes.count
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(id, forKey: .id)
        try container.encode(likes, forKey: .likes)
        try container.encode(photos, forKey: .photos)
        try container.encode(description, forKey: .description)
        try container.encode(comments, forKey: .comments)
        try container.encode(author, forKey: .author)
    }
}

//-------------------------------------------------------------------

struct Author: Hashable {
    let id: String
    let username: String
}

extension Author: Codable {}
