//
//  Comment.swift
//  Fitgram
//
//  Created by Jan Babák on 19.10.2022.
//

import Foundation

struct Comment: Identifiable, Hashable {
    let id: String
    let author: Author
    let postedAt: Date
    let likes: Int
    let text: String
}

extension Comment: Codable {
    enum CodingKeys: String, CodingKey {
        case id
        case author
        case postedAt
        case likes
        case text
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try container.decode(String.self, forKey: .id)
        self.author = try container.decode(Author.self, forKey: .author)
        let dateString = try container.decode(String.self, forKey: .postedAt)
        self.postedAt = try Date(dateString, strategy: Date.ISO8601FormatStyle())
        self.text = try container.decode(String.self, forKey: .text)
        let likes = try container.decode([String].self, forKey: .likes)
        self.likes = likes.count
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(id, forKey: .id)
        try container.encode(author, forKey: .author)
        try container.encode(postedAt, forKey: .postedAt)
        try container.encode(likes, forKey: .likes)
        try container.encode(text, forKey: .text)
    }
}
