//
//  PostsView.swift
//  Fitgram
//
//  Created by Jan Babák on 19.10.2022.
//

import SwiftUI

struct PostsView: View {
    @State var posts: [Post] = []
    
    @State var path = NavigationPath()
    
    var body: some View {
        NavigationStack(path: $path) {
            Group {
                if posts.isEmpty {
                    ProgressView()
                        .progressViewStyle(.circular)
                        .task {
                            await fetchPosts()
                        }
                } else {
                    ScrollView {
                        LazyVGrid(columns: [GridItem()]) {
                            ForEach(posts) { post in
                                PostView(
                                    post: post,
                                    onCommentsButtonTab: {
                                        path.append(post.id)
                                    }
                                )
                                .onTapGesture {
                                    path.append(post)
                                }
                            }
                        }
                    }
                }
            }
            .navigationDestination(for: Post.self) { post in
                PostDetailView(post: post)
            }
            .navigationDestination(for: Post.ID.self, destination: { postID in
                CommentsView(viewModel: .init(postID: postID)) // comments are displaying
            })
            .navigationTitle("Fitgram")
            }
        }
    
    private func fetchPosts() async {
        var request = URLRequest(url: URL(string: "https://fitstagram.ackee.cz/api/feed/")!)
        request.httpMethod = "GET"
        
        do {
            let (data, _) = try await URLSession.shared.data(for: request)
            self.posts = try JSONDecoder().decode([Post].self, from: data)
        } catch {
            print("[ERROR]", error)
        }
    }
}

struct PostsView_Previews: PreviewProvider {
    static var previews: some View {
        PostsView()
    }
}
