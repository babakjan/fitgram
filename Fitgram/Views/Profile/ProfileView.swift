//
//  UserView.swift
//  Fitgram
//
//  Created by Jan Babák on 19.10.2022.
//

import SwiftUI

struct ProfileView: View {
    var user: User
    @AppStorage("username") var username = "babak"
    
    @State var navigationPath = NavigationPath()
    
    var body: some View {
        NavigationStack(path: $navigationPath) {
            ScrollView {
                VStack {
                    //heading
                    Text(username.isEmpty ? user.username : username)
                        .font(.title2)
                        .fontWeight(.bold)
                    
                    //profile info
                    VStack (alignment: .leading) {
                        HStack {
                            Image(user.profilePicture)
                                .resizable()
                                .frame(width: 100, height: 100)
                                .aspectRatio(contentMode: .fit)
                                .clipShape(Circle())
                                .overlay(Circle().strokeBorder(Color.red, lineWidth: 4))
                            Spacer()
                            NumberWithDescription(number: user.posts.count, description: "Posts")
                            Spacer()
                            NumberWithDescription(number: user.followers, description: "Followers")
                            Spacer()
                            NumberWithDescription(number: user.following, description: "Following")
                        }
                        
                        HStack {
                            Text(user.nick)
                                .font(.callout)
                                .fontWeight(.semibold)
                                .multilineTextAlignment(.leading)
                            
                            // edit profile button
                            NavigationLink {
                                EditProfileView()
                            } label: {
                                Image(systemName: "pencil")
                                    .resizable()
                                    .aspectRatio(1, contentMode: .fit)
                                    .frame(height: 20)
                            }
                        }
                        
                        Text(user.description)
                            .font(.callout)
                        
                        HStack {
                            CustomButton(label: "Follow", primary: true)
                            Spacer()
                            CustomButton(label: "Message")
                        }
                    }.padding([.leading, .bottom, .trailing], 16)
                    
                    //image grid
                    LazyVGrid(
                        columns: [
                            GridItem(spacing: 1),
                            GridItem(spacing: 1),
                            GridItem(spacing: 1)
                        ],
                        spacing: 1) {
                            ForEach(0..<22) { i in
                                Rectangle()
                                    .fill(Color(hue: 0.0, saturation: 0.0, brightness: 0.887))
                                    .aspectRatio(1, contentMode: .fill)
                                    .overlay(
                                        AsyncImage(
                                            url: URL(string: "https://placeimg.com/640/480/nature"),
                                            content: { image in
                                                image
                                                    .resizable()
                                                    .scaledToFill()
                                            },
                                            placeholder: {
                                                ProgressView()
                                                    .progressViewStyle(.circular)
                                            })
                                        //RemoteImage(url: URL(string: "https://placeimg.com/640/480/nature")!)
                                    )
                                    .clipped()
                            }
                        }
                }
            }
        }
    }
}

struct NumberWithDescription: View {
    var number: Int
    var description: String
    
    var body: some View {
        VStack {
            Text("\(number)")
                .font(.title3)
                .fontWeight(.semibold)
            Text(description)
                .font(.caption)
                
        }
    }
}

struct CustomButton: View {
    var label: String
    var primary = false
    
    var body: some View {
        if (primary) {
            Button {
            } label: {
                Text(label)
                    .frame(maxWidth: .infinity)
                    .fontWeight(.semibold)
            }
            .buttonStyle(.borderedProminent)
        } else {
            Button {
            } label: {
                Text(label)
                    .frame(maxWidth: .infinity)
                    .fontWeight(.semibold)
            }
            .buttonStyle(.bordered)
        }
    }
}

struct UserView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView(user: User(
            username: "honza_babak",
            nick: "Honza Babák",
            followers: 397,
            following: 122,
            profilePicture: "babakProfilePic",
            description: "🏫 FIT ČVUT -> webdev\n🛹🏓🏌️‍♂️🏀 sports\n🌍Prague Czech Republic\n 23 y/o",
            posts: []))
        .preferredColorScheme(.light)
        
        ProfileView(user: User(
            username: "honza_babak",
            nick: "Honza Babák",
            followers: 397,
            following: 122,
            profilePicture: "babakProfilePic",
            description: "🏫 FIT ČVUT -> webdev\n🛹🏓🏌️‍♂️🏀 sports\n🌍Prague Czech Republic\n 23 y/o",
            posts: []
        ))
        .preferredColorScheme(.dark)
    }
}
