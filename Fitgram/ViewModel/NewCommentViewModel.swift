//
//  NewCommentViewModel.swift
//  Fitgram
//
//  Created by Jan Babák on 19.11.2022.
//

import Foundation
import SwiftUI

struct AddCommentRequest: Encodable {
    let text: String
}

final class NewCommentViewModel: ObservableObject {
    @Binding var isPresented: Bool
    let onNewComment: (String) -> Void
    let postID: Post.ID
    
    @Published var comment = ""
    @Published var isLoading = false
    
    init(
        postID: Post.ID,
        isPresented: Binding<Bool>,
        onNewComment: @escaping (String) -> Void
    ) {
        self.postID = postID
        self._isPresented = isPresented
        self.onNewComment = onNewComment
    }
    
    @MainActor
    func addComment() async {
        isLoading = true
        defer { isLoading = false } // will rand when the function end
        
        guard !comment.isEmpty else { return }
        
        let url = URL(string: "https://fitstagram.ackee.cz/api/feed/\(postID)/comments")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = [
            "Authorization": "babakjan"
        ]
        
        let body = AddCommentRequest(text: comment)
        request.httpBody = try! JSONEncoder().encode(body)
        
        do {
            let (_, response) = try await URLSession.shared.data(for: request)
            print(response)
            onNewComment(comment)
            isPresented = false
        } catch {
            print("[ERROR]", error.localizedDescription)
        }
    }
}
