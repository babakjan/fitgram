//
//  User.swift
//  Fitgram
//
//  Created by Jan Babák on 19.10.2022.
//

import Foundation

struct User {
    var username: String
    var nick: String
    var followers: Int
    var following: Int
    var profilePicture: String
    var description: String
    var posts: [Post]
}
