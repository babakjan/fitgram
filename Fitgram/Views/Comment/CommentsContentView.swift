//
//  CommentsContentView.swift
//  Fitgram
//
//  Created by Jan Babák on 09.11.2022.
//

import SwiftUI

struct CommentsContentView: View {
    let state: CommentsScreenState
    let onNewComment: () -> Void
    
    var body: some View {
        switch state {
        case .loading:
            ProgressView().progressViewStyle(.circular)
        case .error(let error):
            Text(error.localizedDescription)
                .foregroundColor(.red)
        case .comments(let comments):
            List {
                ForEach(comments) { comment in
                    HStack {
                        Text(comment.author.username)
                            .fontWeight(.semibold)
                        Text(comment.text)
                    }
                }
                
                Button(action: {
                    onNewComment()
                }) {
                    Image(systemName: "plus")
                }
            }
            .listStyle(.grouped)
        }
    }
}

struct CommentsContentView_Previews: PreviewProvider {
    static var previews: some View {
        CommentsContentView(
            state: .comments([
            .init(
                id: "1",
                author: Author(id: "1", username: "babakjan"),
                postedAt: try! Date("2022-12-26T18:06:55Z", strategy: Date.ISO8601FormatStyle()),
                likes: 31,
                text: "Pecka!💪"),
            .init(
                id: "2",
                author: Author(id: "1", username: "prod"),
                postedAt: try! Date("2022-12-26T18:06:55Z", strategy: Date.ISO8601FormatStyle()),
                likes: 31,
                text: "dik")
            ]),
            onNewComment: { }
        )
    }
}
