//
//  PostDetailView.swift
//  Fitgram
//
//  Created by Jan Babák on 04.11.2022.
//

import SwiftUI

struct PostDetailView: View {
    var post: Post
    @State var comments: [Comment] = []
    @State var commentsLoaded = false
    @State var showOnlyPhotos = false
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading) {
                //header
                if !showOnlyPhotos {
                    header
                    .padding(.horizontal, 8)
                }
                
                //photos carousal
                phostosCarousal

                if !showOnlyPhotos {
                    underCarousalSection
                }
            }
        }
    }
    
    private var header: some View {
        HStack {
            Text(post.author.username)
                .font(.callout)
                .fontWeight(.semibold)
            
            Spacer()
            
            Button(action: {}) {
                Image(systemName: "ellipsis")
            }
        }
        .transition(.opacity)
    }
    
    private var actionBtns: some View {
        HStack(spacing: 16) {
            Button(action: {}) {
                Image(systemName: "heart")
                    .resizable()
                    .scaledToFit()
                    .frame(height: 24)
            }
            Button(action: {}) {
                Image(systemName: "message")
                    .resizable()
                    .scaledToFit()
                    .frame(height: 24)
            }
            Button(action: {}) {
                Image(systemName: "paperplane")
                    .resizable()
                    .scaledToFit()
                    .frame(height: 24)
            }
            
            Spacer()
            
            Button(action: {}) {
                Image(systemName: "bookmark")
                    .resizable()
                    .scaledToFit()
                    .frame(height: 24)
            }
        }
        .transition(.opacity)
    }
    
    private var phostosCarousal: some View {
        TabView {
            ForEach(post.photos, id: \.self) { photo in
                AsyncImage(url: URL(string: photo)) { image in
                    image.resizable()
                } placeholder: {
                    Rectangle()
                        .fill(Color(hue: 0.0, saturation: 0.0, brightness: 0.887))
                        .overlay {
                            ProgressView()
                                .progressViewStyle(.circular)
                        }
                }
            }
        }
        .tabViewStyle(PageTabViewStyle())
        .frame(height: 400)
        .onTapGesture {
            withAnimation {
                showOnlyPhotos.toggle()
            }
        }
    }
    
    @ViewBuilder
    private var underCarousalSection: some View {
        actionBtns
        .padding(.horizontal, 8)
        .padding(.vertical, 1)
        
        Text("\(post.likes) To se mi líbí")
            .fontWeight(.semibold)
            .padding(.horizontal, 8)
            .padding(.vertical, 1)
            .transition(.opacity)
        
        Group {
            Text(post.author.username)
                .fontWeight(.semibold)
            +
            Text(" " + post.description)
        }
        .padding(.horizontal, 8)
        .transition(.opacity)
        
        //comments
        if !commentsLoaded {
            commentsLoading
        } else {
            commentList
            .padding(.horizontal, 8)
        }
    }
    
    private var commentsLoading: some View {
        HStack() {
            Spacer()
            ProgressView()
                .progressViewStyle(.circular)
                .task {
                    await fetchComments(postId: post.id)
                }
            Spacer()
        }
        .transition(.opacity)
    }
    
    private var commentList: some View {
        VStack(alignment: .leading, spacing: 8) {
            ForEach(comments) { comment in
                CommentView(comment: comment)
                    .padding(.vertical, 2)
            }
        }
        .transition(.opacity)
    }
    
    @MainActor
    private func fetchComments(postId: String) async {
        var request = URLRequest(url: URL(string: "https://fitstagram.ackee.cz/api/feed/\(postId)/comments")!)
        request.httpMethod = "GET"
        
        do {
            let (data, _) = try await URLSession.shared.data(for: request)
            self.comments = try JSONDecoder().decode([Comment].self, from: data)
            self.commentsLoaded = true
        } catch {
            print("[ERROR] ", error)
        }
    }
}

struct PostDetailView_Previews: PreviewProvider {
    static var previews: some View {
        PostDetailView(post: Post(
            id: "E7574AAB-2E6F-43EE-A803-1234953DC36A",
            likes: 3988,
            photos: [
                "https://fitstagram.ackee.cz:443/images/176C9C71-9FB9-47C8-BE0C-27C9700E3A5C.jpg",
                "https://fitstagram.ackee.cz:443/images/17559FD3-35BA-470E-B490-953AFC661A8E.jpg",
                "https://fitstagram.ackee.cz:443/images/5523700B-2E84-4085-AA03-B40B654D2668.jpg"
            ],
            description: "🤌",
            comments: 3,
            author: Author(id: "1", username: "nobody")))
    }
}
