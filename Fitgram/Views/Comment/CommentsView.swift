//
//  CommnetsView.swift
//  Fitgram
//
//  Created by Jan Babák on 19.10.2022.
//

import SwiftUI

struct CommentsView: View {
    @StateObject var viewModel: CommentsViewModel
    
    @State var sheetPresented = false
    
    var body: some View {
        CommentsContentView(
            state: viewModel.state,
            onNewComment: { viewModel.isNewCommentPresented = true
            }
        )
        .refreshable {
            await viewModel.fetchComments()
        }
        .task {
            await viewModel.fetchComments()
        }
        .sheet(isPresented: $viewModel.isNewCommentPresented) {
            NewCommentView(
                viewModel: NewCommentViewModel(
                    postID: viewModel.postID,
                    isPresented: $viewModel.isNewCommentPresented,
                    onNewComment: { _ in
                        Task {
                            await viewModel.fetchComments()
                        }
                    }
                )
            )
        }
    }
}

struct CommentsView_Previews: PreviewProvider {
    static var previews: some View {
        CommentsView(viewModel: .init(postID: "1"))
    }
}
