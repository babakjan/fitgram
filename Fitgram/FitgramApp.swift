//
//  FitgramApp.swift
//  Fitgram
//
//  Created by Jan Babák on 12.10.2022.
//

import SwiftUI

@main
struct FitgramApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
