//
//  CommentView.swift
//  Fitgram
//
//  Created by Jan Babák on 05.11.2022.
//

import SwiftUI

struct CommentView: View {
    let comment: Comment
    let dateFormater = DateFormatter()
    
    init(comment: Comment) {
        self.comment = comment
        dateFormater.dateFormat = "HH:mm, d. M. y"
    }
    
    var body: some View {
        VStack(alignment: .leading, spacing: 4) {
            HStack {
                Group {
                    Text(comment.author.username)
                        .fontWeight(.semibold)
                    +
                    Text(" " + comment.text)
                }
                
                Spacer()
                
                Button(action: {}) {
                    Image(systemName: "heart")
                }
            }
            HStack() {
                Text(dateFormater.string(from: comment.postedAt))
                    .padding(.trailing)
                    
                
                Text("\(comment.likes) likes")
            }
            .font(.caption)
            .foregroundColor(.gray)
        }
    }
}

struct CommentView_Previews: PreviewProvider {
    static var previews: some View {
        CommentView(comment:
            Comment(
                id: "1",
                author: Author(id: "1", username: "babakjan"),
                postedAt: try! Date("2022-12-26T18:06:55Z", strategy: Date.ISO8601FormatStyle()),
                likes: 31,
                text: "Pecka!💪")
        )
    }
}
